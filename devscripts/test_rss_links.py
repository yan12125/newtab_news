import base64
import json
import re
import sys
import time
import xml.etree.ElementTree as ET

import requests
import requests.exceptions
from requests_toolbelt.utils.deprecated import get_encodings_from_content


class InvalidRssException(ValueError):
    pass


class RSSLinksChecker:
    def __init__(self, filename, site):
        with open(filename, 'r') as f:
            self.data = json.loads(f.read())

        self.site = site
        self.problematic_urls = []

    def run(self):
        if not self.site:
            for siteName, siteInfo in self.data.items():
                self.currentSite = siteName
                self.scanSite(siteInfo)
        else:
            self.currentSite = self.site
            self.scanSite(self.data[self.site])

    def scanSite(self, siteInfo):
        if 'args' in siteInfo:
            for arg in siteInfo['args']:
                self.checkUrl(siteInfo['url'].replace('{}', arg))
                time.sleep(1)
        else:
            self.checkUrl(siteInfo['url'])

    def handleError(self, problem, info):
        print(problem)
        info['problem'] = problem
        self.problematic_urls.append(info)

    def checkUrl(self, full_url):
        print(full_url)

        info = {
            'url': full_url,
            'site': self.currentSite,
        }

        try:
            req = requests.get(full_url, allow_redirects=False)
        except requests.exceptions.RequestException as e:
            self.handleError('Can\'t fetch data: ' + str(e), info)
            return

        content = req.content
        info['content'] = base64.b64encode(content).decode('ascii')

        if req.status_code != 200:
            self.handleError('HTTP error code %d' % req.status_code, info)
            return
        else:
            encoding_candidates = (
                [req.encoding] +
                get_encodings_from_content(content) +
                ['UTF-8']
            )
            for encoding in encoding_candidates:
                last_error = None
                try:
                    if not encoding:
                        continue
                    if encoding.lower() == 'utf-8':
                        content = re.sub(b'^\xef\xbb\xbf', b'', content)
                    self.checkRssContent(content.decode(encoding))
                    break
                except (UnicodeDecodeError, InvalidRssException) as e:
                    last_error = e

            if last_error:
                self.handleError('Invalid RSS: ' + str(last_error), info)
                return

    def checkRssContent(self, content):
        try:
            tree = ET.fromstring(content)
        except ET.ParseError:
            raise InvalidRssException('Invalid XML')

        if (not tree.findall('.//item') and
                not tree.findall('.//{http://purl.org/rss/1.0/}item') and
                not tree.findall('.//entry') and
                not tree.findall('.//{http://www.w3.org/2005/Atom}entry')):
            raise InvalidRssException('No item or entry found')


def main():
    site = None

    if len(sys.argv) == 2:
        site = sys.argv[1]

    checker = RSSLinksChecker('../newtab_news/content/siteList.json', site)

    checker.run()

    with open('./check_result.json', 'wb') as f:
        f.write(json.dumps(
            checker.problematic_urls, indent=4
        ).encode('utf-8'))


if __name__ == '__main__':
    main()
