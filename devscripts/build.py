import json
import pathlib

ROOT = pathlib.Path(__file__).resolve().parent.parent


def load_json(filename):
    with open(filename, 'rb') as f:
        data = json.loads(f.read().decode('utf-8'))
    return data


def write_json(filename, data):
    with open(filename, 'wb') as f:
        f.write(json.dumps(data, indent=4).encode('utf-8'))


def build_manifest_json():
    '''
    Build manifest.json for Firefox
    '''
    addon_dir = ROOT / 'newtab_news'
    manifest = load_json(addon_dir / 'manifest.template.json')
    site_list = load_json(addon_dir / 'content' / 'siteList.json')
    sorted_sites = sorted(site_list.items(),
                          key=lambda item: item[1]['description'])
    for name, details in sorted_sites:
        url = details['url']
        if '{}' in url:
            url = url.replace('{}', '*')
        manifest['permissions'].append(url)

    write_json(addon_dir / 'manifest.json', manifest)


def main():
    build_manifest_json()


if __name__ == '__main__':
    main()
