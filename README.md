# Introduction

Firefox addon that shows news from Yahoo! and more sites when open a new tab

This package once supports Google Chrome but now I've removed it. See [chrome branch](https://github.com/yan12125/chrome_newtab/tree/chrome) for the working version of chrome. Note in this version options page is not working.

# Building

Prequisites: Python 3, web-ext installed globally

* Bump the version string in manifest.template.json
* ``python devscripts/sign.py``
