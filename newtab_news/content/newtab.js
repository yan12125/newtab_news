'use strict';

function parseRSS(data) {
    var feedType = 'RSS';

    var items = data.getElementsByTagName('item');

    if (items.length === 0) {
        feedType = 'ATOM';
        items = data.getElementsByTagName('entry');
    }

    // HTMLCollection is read-only
    items = Array.prototype.slice.call(items);

    var idx, temp;
    document.getElementById('title').textContent = data.getElementsByTagName('title')[0].textContent;

    for (let i = items.length - 1; i >= 0; i--) {
        idx = Math.floor(Math.random() * (i + 1));
        temp = items[idx];
        items[idx] = items[i];
        items[i] = temp;
    }

    var count = Math.min(items.length, 5);
    var title, source, link;
    for (let i = 0; i < count; i++) {
        var new_row = document.createElement('div');
        new_row.classList.add('row_news');
        document.getElementById('contents').appendChild(new_row);

        title = items[i].getElementsByTagName('title')[0].textContent;

        link = null;
        if (feedType === 'RSS') {
            link = items[i].getElementsByTagName('link')[0].textContent;
        } else {
            link = items[i].querySelector('link[rel="alternate"]').attributes.href.nodeValue;
        }

        var news_link = document.createElement('a');
        news_link.href = link;
        news_link.textContent = title;
        new_row.appendChild(news_link);

        source = items[i].getElementsByTagName('source');
        if (source.length) {
            var source_info = document.createElement('span');
            source_info.classList.add('info');
            source_info.textContent = source[0].textContent;
            new_row.appendChild(source_info);
        }

        if (i !== count - 1) {
            var separator = document.createElement('div');
            separator.classList.add('sep_line');
            document.getElementById('contents').appendChild(separator);
        }
    }
}

async function loadNews() {
    let rss_url_info = null;
    try {
        rss_url_info = await getRssUrl();
        let buffer = await (await Promise.race([
            fetch(rss_url_info.url),
            new Promise(function (resolve, reject) {
                setTimeout(() => reject(new Error('timeout')), 2000);
            })
        ])).arrayBuffer();
        var encoding = rss_url_info.encoding || 'utf-8';
        var decoder = new TextDecoder(encoding);
        var parser = new DOMParser();
        var content = decoder.decode(buffer);
        // From youtube_dl/utils.py, fix_xml_ampersands
        content = content.replace(
            /&(?!amp;|lt;|gt;|apos;|quot;|#x[0-9a-fA-F]{0,4};|#[0-9]{0,4};)/, '&amp;')
        parseRSS(parser.parseFromString(content, 'text/xml'));

        document.getElementById('source_link_div').style.display = 'block';
        let source_link = document.getElementById('source_link');
        source_link.href = rss_url_info.url;
        source_link.innerText = rss_url_info.description;
    } catch (error) {
        let errMsg = error;
        let errMsg_HTML = error;
        if (rss_url_info) {
            errMsg = `無法取得${rss_url_info.url}: ${error}`;
            errMsg_HTML = document.createElement('span');
            errMsg_HTML.innerHTML = `無法取得<a href="${rss_url_info.url}">${rss_url_info.url}</a>: ${error}`;
        }
        printHTMLError(errMsg_HTML);
        browser.storage.local.set({'last_error': errMsg});
        console.log(errMsg);
    }
};

document.addEventListener('DOMContentLoaded', loadNews);
