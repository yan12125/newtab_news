'use strict';

async function initOptions() {
    let siteList = await getSite({all: true});
    let siteOptions = document.getElementById('siteOptions');
    for (let site in siteList) {
        if (!siteList.hasOwnProperty(site)) {
            continue;
        }
        let option = document.createElement('input');
        option.type = 'checkbox';
        option.name = site;
        option.checked = siteList[site].enabled;
        option.addEventListener('change', function (evt) {
            browser.storage.local.set({ [propForSite(this.name)]: this.checked });
        });
        siteOptions.appendChild(option);
        siteOptions.appendChild(document.createTextNode(siteList[site].description));
        siteOptions.appendChild(document.createElement('br'));
    }
}

function toggle_all(enabled) {
    let checkboxes = Array.from(document.querySelectorAll('#siteOptions input[type="checkbox"]'));

    for (let idx in checkboxes) {
        let checkbox = checkboxes[idx];
        checkbox.checked = enabled;
        checkbox.dispatchEvent(new Event('change'));
    }
}

document.addEventListener('DOMContentLoaded', function () {
    initOptions();
    document.getElementById('enableAll').addEventListener('click', () => toggle_all(true));
    document.getElementById('disableAll').addEventListener('click', () => toggle_all(false));
});
