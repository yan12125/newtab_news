'use strict';

Array.prototype.random_item = function () {
    return this[Math.floor(Math.random() * this.length)];
};

var printHTMLError = function (node) {
    let newline = document.createElement('br');
    let messages = document.getElementById('messages');
    messages.appendChild(node);
    messages.appendChild(newline);
};

var printError = function (msg) {
    let content = document.createTextNode(msg);
    printHTMLError(content);
};
