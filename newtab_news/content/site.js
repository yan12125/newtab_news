'use strict';

function propForSite(siteName) {
    return `site.${siteName}`;
}

async function getSite(params = {}) {
    let siteList = await (await fetch('siteList.json')).json();

    // check whether sites are enabled or not
    let results = await browser.storage.local.get(Object.keys(siteList).map(propForSite));
    for (let siteName in siteList) {
        let propName = propForSite(siteName);
        // if a site is missing in the storage (a new site), make it enabled
        if (!results.hasOwnProperty(propName)) {
            results[propName] = true;
            browser.storage.local.set({ [propName]: true });
        }
        siteList[siteName].enabled = results[propName];
    }
    if (params.hasOwnProperty('all') && params.all === true) {
        return siteList;
    }
    let sitesEnabled = Object.entries(siteList).filter( ([siteName, siteDetails]) => siteDetails.enabled );
    if (!sitesEnabled.length) {
        throw new Error("No sites are enabled!");
    }
    return sitesEnabled.random_item();
}

async function getRssUrl() {
    let [siteName, siteDetails] = await getSite();
    var url = siteDetails.url;
    if (siteDetails.hasOwnProperty('args')) {
        url = url.replace('{}', siteDetails.args.random_item());
    }
    return {
        url: url,
        encoding: siteDetails.encoding,
        description: siteDetails.description,
    };
}
